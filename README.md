# Lab7 -- Stress testing 

Gleb Petrakov (g.petrakov@innopolis.ru)

## Run

### Apache benchmark

All scripts for apache benchmark located at `scripts/`.  
You need Docker to run these scripts.

Example:  
With shell: `./scripts/ab_spec.sh`  
With npm: `npm run ab_spec`


### Artillery

Install dependencies:  
`npm ci`

Run script:
`npm run artillery`

Reports are located at `reports/`

## Reports

### Apache benchmark

#### getSpec
![getSpec](img/ab_spec.png)

#### calculatePrice fixed
![calculatePrice fixed](img/ab_fixed.png)

#### calculatePrice minute
![calculatePrice minute](img/ab_minute.png)

### Artillery

![artillery1](img/artillery1.png)
![artillery2](img/artillery2.png)
