#!/usr/bin/env sh

set -x

docker run --rm httpd:2.4 ab -m "GET" "https://script.google.com/macros/s/AKfycbwY9rQGIezJ1kyEo9SF6ShgeqjqvJGkp0UoULx5owyGBi-1JvmJI3ePzg/exec?service=calculatePrice&email=g.petrakov@innopolis.ru&type=luxury&plan=minute&distance=100&planned_distance=100&time=110&planned_time=100&inno_discount=yes"